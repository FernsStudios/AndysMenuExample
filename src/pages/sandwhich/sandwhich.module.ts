import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SandwhichPage } from './sandwhich';

@NgModule({
  declarations: [
    SandwhichPage,
  ],
  imports: [
    IonicPageModule.forChild(SandwhichPage),
  ],
})
export class SandwhichPageModule {}
