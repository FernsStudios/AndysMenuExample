import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {}
  gotoBreakfastPage(){this.navCtrl.push('BreakfastPage')};
  gotoBurgerPage(){this.navCtrl.push('BurgersPage')};
  gotoCoffeePage(){this.navCtrl.push('CoffeePage')};
  gotoiceCreamPage(){this.navCtrl.push('IceCreamPage')};
  gotoSandwhichPage(){this.navCtrl.push('SandwhichPage')};
  gotoSpecialitiesPage(){this.navCtrl.push('SpecialitiesPage')};
}
