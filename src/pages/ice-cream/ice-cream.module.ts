import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IceCreamPage } from './ice-cream';

@NgModule({
  declarations: [
    IceCreamPage,
  ],
  imports: [
    IonicPageModule.forChild(IceCreamPage),
  ],
})
export class IceCreamPageModule {}
